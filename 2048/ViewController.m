//
//  ViewController.m
//  2048
//
//  Created by Corey Hom on 2/1/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController;

@synthesize welcome;
@synthesize t00, t01, t02, t03, t04, t05, t06, t07, t08, t09, t10, t11, t12, t13, t14, t15;
@synthesize score, scoreCount;
@synthesize tiles;
@synthesize winner;
@synthesize giveUp;
int counter = 0;

-(IBAction) pressButton:(id)sender
{
    switch ((int)[sender tag])
    {
        case 5:
            NSLog(@"Pressed Give Up %d", (int)[sender tag]);
            giveUp.hidden = NO;
            break;
        case 6:
            NSLog(@"I Wanna Feel Better %d", (int)[sender tag]);
            giveUp.hidden = YES;
            winner.hidden = NO;
            break;
        case 0:
            NSLog(@"Pressed Up %d", (int)[sender tag]);
            bool action = false;
            //FIRST COLUMN UP
            while (t00.text.intValue == 0)
            {
                [t00 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0 && t04.text.intValue == 0 && t08.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t04.text.intValue == 0)
            {
                [t04 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t08.text.intValue == 0 && t12.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t08.text.intValue == 0)
            {
                [t08 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t12.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t00.text.intValue == t04.text.intValue)
            {
                int changeValue = t00.text.intValue * 2;
                [t00 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t04.text.intValue == t08.text.intValue)
            {
                int changeValue = t04.text.intValue * 2;
                [t04 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t08.text.intValue == t12.text.intValue)
            {
                int changeValue = t08.text.intValue * 2;
                [t08 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //SECOND COLUMN UP
            while (t01.text.intValue == 0)
            {
                [t01 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t01.text.intValue == 0 && t05.text.intValue == 0 && t09.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t05.text.intValue == 0)
            {
                [t05 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t09.text.intValue == 0 && t13.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t09.text.intValue == 0)
            {
                [t09 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t13.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t01.text.intValue == t05.text.intValue)
            {
                int changeValue = t01.text.intValue * 2;
                [t01 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t05.text.intValue == t09.text.intValue)
            {
                int changeValue = t05.text.intValue * 2;
                [t05 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t09.text.intValue == t13.text.intValue)
            {
                int changeValue = t09.text.intValue * 2;
                [t09 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //THIRD COLUMN UP
            while (t02.text.intValue == 0)
            {
                [t02 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t02.text.intValue == 0 && t06.text.intValue == 0 && t10.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t06.text.intValue == 0)
            {
                [t06 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t10.text.intValue == 0 && t14.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t10.text.intValue == 0)
            {
                [t10 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t14.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t02.text.intValue == t06.text.intValue)
            {
                int changeValue = t02.text.intValue * 2;
                [t02 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t06.text.intValue == t10.text.intValue)
            {
                int changeValue = t06.text.intValue * 2;
                [t06 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t10.text.intValue == t14.text.intValue)
            {
                int changeValue = t10.text.intValue * 2;
                [t10 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //4TH COLUMN UP
            while (t03.text.intValue == 0)
            {
                [t03 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t03.text.intValue == 0 && t07.text.intValue == 0 && t11.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t07.text.intValue == 0)
            {
                [t07 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t11.text.intValue == 0 && t15.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t11.text.intValue == 0)
            {
                [t11 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t15.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t03.text.intValue == t07.text.intValue)
            {
                int changeValue = t03.text.intValue * 2;
                [t03 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t07.text.intValue == t11.text.intValue)
            {
                int changeValue = t07.text.intValue * 2;
                [t07 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t11.text.intValue == t15.text.intValue)
            {
                int changeValue = t11.text.intValue * 2;
                [t11 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //LOGIC TO ADD ANOTHER NUMBER
            bool loopCheck = true;
            if (action == true)
            {
                while (loopCheck){
                    int addNum = arc4random_uniform(15);
                    if (((UILabel *)[tiles objectAtIndex: addNum]).text.intValue == 0)
                    {
                        if (scoreCount.text.intValue > 2000)
                        {
                            int add4Chance = arc4random_uniform(99);
                            if (add4Chance > 15)
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                            }
                            else
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 4]];
                            }
                        }
                        else
                        {
                            [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                        }
                        loopCheck = false;
                    }
                    else
                    {
                        addNum = arc4random_uniform(15);
                    }
                }
                for (UILabel *label in tiles)
                {
                    counter += label.text.intValue;
                }
                [scoreCount setText: [NSString stringWithFormat:@"%d", counter]];
                action = false;
            }
            break;
        case 1:
            NSLog(@"Pressed Left %d", (int)[sender tag]);
            action = false;
            //FIST ROW LEFT
            while (t00.text.intValue == 0)
            {
                [t00 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0 && t01.text.intValue == 0 && t02.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t01.text.intValue == 0)
            {
                [t01 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t02.text.intValue == 0 && t03.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t02.text.intValue == 0)
            {
                [t02 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t03.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t00.text.intValue == t01.text.intValue)
            {
                int changeValue = t00.text.intValue * 2;
                [t00 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t01.text.intValue == t02.text.intValue)
            {
                int changeValue = t01.text.intValue * 2;
                [t01 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t02.text.intValue == t03.text.intValue)
            {
                int changeValue = t02.text.intValue * 2;
                [t02 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //SECOND ROW LEFT
            while (t04.text.intValue == 0)
            {
                [t04 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t04.text.intValue == 0 && t05.text.intValue == 0 && t06.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t05.text.intValue == 0)
            {
                [t05 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t06.text.intValue == 0 && t07.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t06.text.intValue == 0)
            {
                [t06 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t07.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t04.text.intValue == t05.text.intValue)
            {
                int changeValue = t04.text.intValue * 2;
                [t04 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t05.text.intValue == t06.text.intValue)
            {
                int changeValue = t05.text.intValue * 2;
                [t05 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t06.text.intValue == t07.text.intValue)
            {
                int changeValue = t06.text.intValue * 2;
                [t06 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //THIRD ROW LEFT
            while (t08.text.intValue == 0)
            {
                [t08 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t08.text.intValue == 0 && t09.text.intValue == 0 && t10.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t09.text.intValue == 0)
            {
                [t09 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t10.text.intValue == 0 && t11.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t10.text.intValue == 0)
            {
                [t10 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t11.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t08.text.intValue == t09.text.intValue)
            {
                int changeValue = t08.text.intValue * 2;
                [t08 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t09.text.intValue == t10.text.intValue)
            {
                int changeValue = t09.text.intValue * 2;
                [t09 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t10.text.intValue == t11.text.intValue)
            {
                int changeValue = t10.text.intValue * 2;
                [t10 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //FOURTH ROW LEFT
            while (t12.text.intValue == 0)
            {
                [t12 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t12.text.intValue == 0 && t13.text.intValue == 0 && t14.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t13.text.intValue == 0)
            {
                [t13 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t14.text.intValue == 0 && t15.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t14.text.intValue == 0)
            {
                [t14 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t15.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t12.text.intValue == t13.text.intValue)
            {
                int changeValue = t12.text.intValue * 2;
                [t12 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t13.text.intValue == t14.text.intValue)
            {
                int changeValue = t14.text.intValue * 2;
                [t13 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t15.text.intValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t14.text.intValue == t15.text.intValue)
            {
                int changeValue = t14.text.intValue * 2;
                [t14 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t15 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //LOGIC TO ADD ANOTHER NUMBER
            loopCheck = true;
            if (action == true)
            {
                while (loopCheck){
                    int addNum = arc4random_uniform(15);
                    if (((UILabel *)[tiles objectAtIndex: addNum]).text.intValue == 0)
                    {
                        if (scoreCount.text.intValue > 2000)
                        {
                            int add4Chance = arc4random_uniform(99);
                            if (add4Chance > 15)
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                            }
                            else
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 4]];
                            }
                        }
                        else
                        {
                            [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                        }
                        loopCheck = false;
                    }
                    else
                    {
                        addNum = arc4random_uniform(15);
                    }
                }
                for (UILabel *label in tiles)
                {
                    counter += label.text.intValue;
                }
                [scoreCount setText: [NSString stringWithFormat:@"%d", counter]];
                action = false;
            }
            break;
        case 2:
            NSLog(@"Pressed Right %d", (int)[sender tag]);
            action = false;
            //FIST ROW RIGHT
            while (t03.text.intValue == 0)
            {
                [t03 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0 && t01.text.intValue == 0 && t02.text.intValue == 0 && t03.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t02.text.intValue == 0)
            {
                [t02 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t01.text.intValue == 0 && t00.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t01.text.intValue == 0)
            {
                [t01 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t03.text.intValue == t02.text.intValue)
            {
                int changeValue = t03.text.intValue * 2;
                [t03 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t02.text.intValue == t01.text.intValue)
            {
                int changeValue = t02.text.intValue * 2;
                [t02 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t01.text.intValue == t00.text.intValue)
            {
                int changeValue = t01.text.intValue * 2;
                [t01 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //SECOND ROW RIGHT
            while (t07.text.intValue == 0)
            {
                [t07 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t04.text.intValue == 0 && t05.text.intValue == 0 && t06.text.intValue == 0 && t07.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t06.text.intValue == 0)
            {
                [t06 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t05.text.intValue == 0 && t04.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t05.text.intValue == 0)
            {
                [t05 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t04.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t07.text.intValue == t06.text.intValue)
            {
                int changeValue = t07.text.intValue * 2;
                [t07 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t06.text.intValue == t05.text.intValue)
            {
                int changeValue = t06.text.intValue * 2;
                [t06 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t05.text.intValue == t04.text.intValue)
            {
                int changeValue = t05.text.intValue * 2;
                [t05 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //THIRD ROW RIGHT
            while (t11.text.intValue == 0)
            {
                [t11 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t08.text.intValue == 0 && t09.text.intValue == 0 && t10.text.intValue ==0)
                {
                    break;
                }
                action = true;
            }
            while (t10.text.intValue == 0)
            {
                [t10 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t09.text.intValue == 0 && t08.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t09.text.intValue == 0)
            {
                [t09 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t08.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t11.text.intValue == t10.text.intValue)
            {
                int changeValue = t11.text.intValue * 2;
                [t11 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t10.text.intValue == t09.text.intValue)
            {
                int changeValue = t10.text.intValue * 2;
                [t10 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t09.text.intValue == t08.text.intValue)
            {
                int changeValue = t09.text.intValue * 2;
                [t09 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //FOURTH ROW RIGHT
            while (t15.text.intValue == 0)
            {
                [t15 setText:[NSString stringWithFormat:@"%d", t14.text.intValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t12.text.intValue == 0 && t13.text.intValue == 0 && t14.text.intValue == 0 && t15.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t14.text.intValue == 0)
            {
                [t14 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t13.text.intValue == 0 && t12.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t13.text.intValue == 0)
            {
                [t13 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t12.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t15.text.intValue == t14.text.intValue)
            {
                int changeValue = t15.text.intValue * 2;
                [t15 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t14 setText:[NSString stringWithFormat:@"%d", t13.text.intValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t14.text.intValue == t13.text.intValue)
            {
                int changeValue = t14.text.intValue * 2;
                [t14 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t13 setText:[NSString stringWithFormat:@"%d", t12.text.intValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t13.text.intValue == t12.text.intValue)
            {
                int changeValue = t13.text.intValue * 2;
                [t13 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t12 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //LOGIC TO ADD ANOTHER NUMBER
            loopCheck = true;
            if (action == true)
            {
                while (loopCheck){
                    int addNum = arc4random_uniform(15);
                    if (((UILabel *)[tiles objectAtIndex: addNum]).text.intValue == 0)
                    {
                        if (scoreCount.text.intValue > 2000)
                        {
                            int add4Chance = arc4random_uniform(99);
                            if (add4Chance > 15)
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                            }
                            else
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 4]];
                            }
                        }
                        else
                        {
                            [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                        }
                        loopCheck = false;
                    }
                    else
                    {
                        addNum = arc4random_uniform(15);
                    }
                }
                for (UILabel *label in tiles)
                {
                    counter += label.text.intValue;
                }
                [scoreCount setText: [NSString stringWithFormat:@"%d", counter]];
                action = false;
            }
            break;
        case 3:
            NSLog(@"Pressed Down %d", (int)[sender tag]);
            action = false;
            //FIRST COLUMN DOWN
            while (t12.text.intValue == 0)
            {
                [t12 setText:[NSString stringWithFormat:@"%d", t08.text.intValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0 && t04.text.intValue == 0 && t08.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t08.text.intValue == 0)
            {
                [t08 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t04.text.intValue == 0 && t00.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t04.text.intValue == 0)
            {
                [t04 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t00.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t12.text.intValue == t08.text.intValue)
            {
                int changeValue = t12.text.intValue * 2;
                [t12 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t08 setText:[NSString stringWithFormat:@"%d", t04.text.intValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t08.text.intValue == t04.text.intValue)
            {
                int changeValue = t04.text.intValue * 2;
                [t08 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t04 setText:[NSString stringWithFormat:@"%d", t00.text.intValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t04.text.intValue == t00.text.intValue)
            {
                int changeValue = t04.text.intValue * 2;
                [t04 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t00 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //SECOND COLUMN DOWN
            while (t13.text.intValue == 0)
            {
                [t13 setText:[NSString stringWithFormat:@"%d", t09.text.intValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t01.text.intValue == 0 && t05.text.intValue == 0 && t09.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t09.text.intValue == 0)
            {
                [t09 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t01.text.intValue == 0 && t05.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t05.text.intValue == 0)
            {
                [t05 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t01.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t13.text.intValue == t09.text.intValue)
            {
                int changeValue = t13.text.intValue * 2;
                [t13 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t09 setText:[NSString stringWithFormat:@"%d", t05.text.intValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t09.text.intValue == t05.text.intValue)
            {
                int changeValue = t09.text.intValue * 2;
                [t09 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t05 setText:[NSString stringWithFormat:@"%d", t01.text.intValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t05.text.intValue == t01.text.intValue)
            {
                int changeValue = t05.text.intValue * 2;
                [t05 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t01 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //THIRD COLUMN DOWN
            while (t14.text.intValue == 0)
            {
                [t14 setText:[NSString stringWithFormat:@"%d", t10.text.intValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t02.text.intValue == 0 && t06.text.intValue == 0 && t10.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t10.text.intValue == 0)
            {
                [t10 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t06.text.intValue == 0 && t02.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t06.text.intValue == 0)
            {
                [t06 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t02.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t14.text.intValue == t10.text.intValue)
            {
                int changeValue = t14.text.intValue * 2;
                [t14 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t10 setText:[NSString stringWithFormat:@"%d", t06.text.intValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t10.text.intValue == t06.text.intValue)
            {
                int changeValue = t10.text.intValue * 2;
                [t10 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t06 setText:[NSString stringWithFormat:@"%d", t02.text.intValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t06.text.intValue == t02.text.intValue)
            {
                int changeValue = t06.text.intValue * 2;
                [t06 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t02 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //4TH COLUMN DOWN
            while (t15.text.intValue == 0)
            {
                [t15 setText:[NSString stringWithFormat:@"%d", t11.text.intValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t03.text.intValue == 0 && t07.text.intValue == 0 && t11.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t11.text.intValue == 0)
            {
                [t11 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t07.text.intValue == 0 && t03.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            while (t07.text.intValue == 0)
            {
                [t07 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                if (t03.text.intValue == 0)
                {
                    break;
                }
                action = true;
            }
            if (t15.text.intValue == t11.text.intValue)
            {
                int changeValue = t15.text.intValue * 2;
                [t15 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t11 setText:[NSString stringWithFormat:@"%d", t07.text.intValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t11.text.intValue == t07.text.intValue)
            {
                int changeValue = t11.text.intValue * 2;
                [t11 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t07 setText:[NSString stringWithFormat:@"%d", t03.text.intValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            if (t07.text.intValue == t03.text.intValue)
            {
                int changeValue = t07.text.intValue * 2;
                [t07 setText:[NSString stringWithFormat:@"%d", changeValue]];
                [t03 setText:[NSString stringWithFormat:@"%d", 0]];
                action = true;
            }
            //LOGIC TO ADD ANOTHER NUMBER
            loopCheck = true;
            if (action == true)
            {
                while (loopCheck){
                    int addNum = arc4random_uniform(15);
                    if (((UILabel *)[tiles objectAtIndex: addNum]).text.intValue == 0)
                    {
                        if (scoreCount.text.intValue > 2000)
                        {
                            int add4Chance = arc4random_uniform(99);
                            if (add4Chance > 15)
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                            }
                            else
                            {
                                [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 4]];
                            }
                        }
                        else
                        {
                            [[tiles objectAtIndex:addNum]setText:[NSString stringWithFormat:@"%d", 2]];
                        }
                        loopCheck = false;
                    }
                    else
                    {
                        addNum = arc4random_uniform(15);
                    }
                }
                for (UILabel *label in tiles)
                {
                    counter += label.text.intValue;
                }
                [scoreCount setText: [NSString stringWithFormat:@"%d", counter]];
                action = false;
            }
            break;
        case 4:
            NSLog(@"Pressed Start %d", (int)[sender tag]);
            float randomRed = arc4random_uniform(255);
            float randomGreen = arc4random_uniform(255);
            float randomBlue = arc4random_uniform(255);
            //CLEAR THE BOARD
            giveUp.hidden = YES;
            winner.hidden = YES;
            counter = 0;
            [scoreCount setText: [NSString stringWithFormat:@"%d", 0]];
            for (UILabel *label in tiles)
            {
                [label setText: [NSString stringWithFormat:@"%d", 0]];
                label.textColor = [UIColor colorWithRed:randomRed/255.0 green:randomGreen/255.0 blue:randomBlue/255.0 alpha:1];
            }
            //Percentage, sometimes start with 4 or 2. So let's make it random.
            int randNum = arc4random_uniform(99);
            NSMutableArray *pickable = [NSMutableArray arrayWithObjects: @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15, nil];
            if (randNum > 50)
            {
                int starting1 = arc4random_uniform(15);
                [[tiles objectAtIndex:starting1]setText:[NSString stringWithFormat:@"%d", 4]];
                [pickable removeObjectAtIndex: starting1];
                int starting2 = arc4random_uniform(14);
                [[tiles objectAtIndex:starting2]setText: [NSString stringWithFormat:@"%d", 2]];
            }
            else{
                int starting1 = arc4random_uniform(15);
                [[tiles objectAtIndex:starting1]setText:[NSString stringWithFormat:@"%d", 2]];
                [pickable removeObjectAtIndex: starting1];
                int starting2 = arc4random_uniform(14);
                [[tiles objectAtIndex:starting2]setText: [NSString stringWithFormat:@"%d", 2]];
            }
            break;
    }
    for (UILabel *label in tiles)
    {
        if (label.text.intValue == 2048)
        {
            winner.hidden = NO;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
