//
//  ViewController.h
//  2048
//
//  Created by Corey Hom on 2/1/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *welcome;
@property (strong, nonatomic) IBOutlet UILabel *t00;
@property (strong, nonatomic) IBOutlet UILabel *t01;
@property (strong, nonatomic) IBOutlet UILabel *t02;
@property (strong, nonatomic) IBOutlet UILabel *t03;
@property (strong, nonatomic) IBOutlet UILabel *t04;
@property (strong, nonatomic) IBOutlet UILabel *t05;
@property (strong, nonatomic) IBOutlet UILabel *t06;
@property (strong, nonatomic) IBOutlet UILabel *t07;
@property (strong, nonatomic) IBOutlet UILabel *t08;
@property (strong, nonatomic) IBOutlet UILabel *t09;
@property (strong, nonatomic) IBOutlet UILabel *t10;
@property (strong, nonatomic) IBOutlet UILabel *t11;
@property (strong, nonatomic) IBOutlet UILabel *t12;
@property (strong, nonatomic) IBOutlet UILabel *t13;
@property (strong, nonatomic) IBOutlet UILabel *t14;
@property (strong, nonatomic) IBOutlet UILabel *t15;

@property (strong, nonatomic) IBOutlet UILabel *score;
@property (strong, nonatomic) IBOutlet UILabel *scoreCount;

@property (strong, nonatomic) IBOutlet UIImageView *winner;
@property (strong, nonatomic) IBOutlet UIImageView *giveUp;
-(IBAction) pressButton:(id)sender;

@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *tiles;

@end

