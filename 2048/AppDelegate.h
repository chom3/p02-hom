//
//  AppDelegate.h
//  2048
//
//  Created by Corey Hom on 2/1/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

