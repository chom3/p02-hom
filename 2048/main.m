//
//  main.m
//  2048
//
//  Created by Corey Hom on 2/1/16.
//  Copyright © 2016 Corey Hom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
